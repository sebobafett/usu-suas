#!/bin/bash

# rosflight
cd ~/plane/src
git clone --recurse-submodules https://github.com/rosflight/rosflight.git
cd rosflight
rosdep install --ignore-src rosflight
cd ~/plane/src
rosdep install --ignore-src --from-path rosflight
cd ~/plane
catkin_make

# plotter
echo -e "${GN}----- Cloning Plotter -----${NC}"
cd ~/plane/src
git clone https://gitlab.com/Randall.Christensen/plotter.git
cd ~/plane
catkin_make


# start_ros_flight
echo -e "${GN}----- Cloning Start_ros_flight -----${NC}"
cd ~/plane/src
git clone https://gitlab.com/Randall.Christensen/start_ros_flight.git
cd start_ros_flight
mv parameters.yml calibrate_sensors ~
#mv drone_simple.yaml ~/catkin_ws/src/ublox/ublox_gps/config
cd ~/plane
catkin_make

#### Misc  ####
rospack profile
source ~/.bashrc
echo -e "${GN}----- Upgrading -----${NC}"
sudo apt upgrade
echo -e "${GN}----- Cleaning -----${NC}"
sudo apt clean
source ~/.bashrc
