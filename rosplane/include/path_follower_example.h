#ifndef PATH_FOLLOWER_EXAMPLE_H
#define PATH_FOLLOWER_EXAMPLE_H

#include "path_follower_base.h"
#include <Eigen/Eigen>


#define M_PI_F 3.14159265358979323846f
#define M_PI_2_F 1.57079632679489661923f
#define M_2_PI_F 6.28318530718f

namespace rosplane
{

class path_follower_example : public path_follower_base
{
public:
  path_follower_example();
private:
  virtual void follow(const struct params_s &params, const struct input_s &input, struct output_s &output);
  void limPi(float input,float &change);
  void limPi(float &input);
};

} //end namespace
#endif // PATH_FOLLOWER_EXAMPLE_H
