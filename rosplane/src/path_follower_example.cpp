#include "path_follower_example.h"


namespace rosplane
{

path_follower_example::path_follower_example()
{
}

void path_follower_example::follow(const params_s &params, const input_s &input, output_s &output)
{
    float h_c = -100;
    float chi_c = 0;

     // implement line following and orbit following in Chapter 10 
     // Straight line following
    if(input.p_type == path_type::Line) // Maybe change orbit to 1
    {  
        // Example constructors
        // Eigen::Vector3f c_rs = p_s + R*rotz(M_PI_2_F)*qcs;
        // Eigen::Vector3f qce(cce,sce,0);
        // Compute h_c 
        Eigen::Vector3f k(0.0, 0.0, -1.0);
        
        Eigen::Vector3f p;
        p << input.pn, input.pe, -input.h;

        Eigen::Vector3f r(input.r_path[0],input.r_path[1],input.r_path[2]);
        Eigen::Vector3f q(input.q_path[0],input.q_path[1],input.q_path[2]);

        Eigen::Vector3f e_i = p-r;
        Eigen::Vector3f n = (q.cross(k)).normalized();
        Eigen::Vector3f s = e_i - (e_i.dot(n)) * n;
        
        h_c = -r[2] + hypot(s[0],s[1])*(q[2]/(hypot(q[0],q[1])));
        
        // Compute chi_c
        float chi_q = atan2(input.q_path[1],input.q_path[0]);
        limPi(input.chi,chi_q);
        float e_crosstrack = -sin(chi_q)*(input.pn-input.r_path[0])+cos(chi_q)*(input.pe-input.r_path[1]);
        chi_c = chi_q - params.chi_infty/M_PI_2_F*atan(params.k_path*e_crosstrack);
    }
    // Orbit following
    else if(input.p_type == path_type::Orbit) // Maybe change orbit to 0
    {
        float d = hypot(input.pn-input.c_orbit[0],input.pe-input.c_orbit[1]);
        float phi = atan2(input.pe-input.c_orbit[1],input.pn-input.c_orbit[0]);
        
        h_c = -input.c_orbit[2];

        limPi(input.chi,phi);
        
        chi_c = phi + input.lam_orbit*(M_PI_2_F+atan(params.k_orbit*(d-input.rho_orbit)/input.rho_orbit));
    }
    else
    {
        output.h_c = input.h;
        output.chi_c = input.chi;
    }
    limPi(chi_c);
    output.h_c = h_c;
    output.chi_c = chi_c;
    output.phi_ff = 0;
    output.Va_c = input.Va_d;
}

void path_follower_example::limPi(float &input)
{
    limPi(0, input);
}

void path_follower_example::limPi(float input,float &change)
{
    while(change-input < - M_PI_F)
    {
        change = change + M_2_PI_F;
    }
    while(change-input > M_PI_F)
    {
        change = change - M_2_PI_F;
    }    
}

} //end namespace
