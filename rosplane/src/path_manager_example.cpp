#include "path_manager_example.h"
#include "ros/ros.h"
#include <cmath>

namespace rosplane
{

path_manager_example::path_manager_example() : path_manager_base()
{
  fil_state_ = fillet_state::STRAIGHT;
  dub_state_ = dubin_state::FIRST;
}

void path_manager_example::manage(const params_s &params, const input_s &input, output_s &output)
{

  if (num_waypoints_ < 2)
  {
    ROS_WARN_THROTTLE(4, "No waypoints received! Loitering about origin at 50m");
    output.flag = false;
    output.Va_d = 12;
    output.c[0] = 0.0f;
    output.c[1] = 0.0f;
    output.c[2] = -50.0f;
    output.rho = params.R_min;
    output.lambda = 1;
  }
  else
  {
    ROS_WARN_THROTTLE(100, "Waypoints received! I am following");
    if (waypoints_[idx_a_].chi_valid)
    {
      manage_dubins(params, input, output);
    }
    else
    {
      /** Switch the following for flying directly to waypoints, or filleting corners */
      //manage_line(params, input, output);
      manage_fillet(params, input, output);
    }
  }
}

void path_manager_example::manage_line(const params_s &params, const input_s &input, output_s &output)
{

  Eigen::Vector3f p;
  p << input.pn, input.pe, -input.h;

  int idx_b;
  int idx_c;
  if (idx_a_ == num_waypoints_ - 1)
  {
    idx_b = 0;
    idx_c = 1;
  }
  else if (idx_a_ == num_waypoints_ - 2)
  {
    idx_b = num_waypoints_ - 1;
    idx_c = 0;
  }
  else
  {
    idx_b = idx_a_ + 1;
    idx_c = idx_b + 1;
  }

  Eigen::Vector3f w_im1(waypoints_[idx_a_].w);
  Eigen::Vector3f w_i(waypoints_[idx_b].w);
  Eigen::Vector3f w_ip1(waypoints_[idx_c].w);

  output.flag = true;
  output.Va_d = waypoints_[idx_a_].Va_d;
  output.r[0] = w_im1(0);
  output.r[1] = w_im1(1);
  output.r[2] = w_im1(2);
  Eigen::Vector3f q_im1 = (w_i - w_im1).normalized();
  Eigen::Vector3f q_i = (w_ip1 - w_i).normalized();
  output.q[0] = q_im1(0);
  output.q[1] = q_im1(1);
  output.q[2] = q_im1(2);

  Eigen::Vector3f n_i = (q_im1 + q_i).normalized();
  if ((p - w_i).dot(n_i) > 0.0f)
  {
    if (idx_a_ == num_waypoints_ - 1)
      idx_a_ = 0;
    else
      idx_a_++;
  }

}

void path_manager_example::manage_fillet(const params_s &params, const input_s &input, output_s &output)
{
  if (num_waypoints_ < 3) //since it fillets don't make sense between just two points
  {
    manage_line(params, input, output);
    return;
  }

  Eigen::Vector3f p;
  p << input.pn, input.pe, -input.h;

  int idx_b;
  int idx_c;
  if (idx_a_ == num_waypoints_ - 1)
  {
    idx_b = 0;
    idx_c = 1;
  }
  else if (idx_a_ == num_waypoints_ - 2)
  {
    idx_b = num_waypoints_ - 1;
    idx_c = 0;
  }
  else
  {
    idx_b = idx_a_ + 1;
    idx_c = idx_b + 1;
  }

  Eigen::Vector3f w_im1(waypoints_[idx_a_].w);
  Eigen::Vector3f w_i(waypoints_[idx_b].w);
  Eigen::Vector3f w_ip1(waypoints_[idx_c].w);

  float R_min = params.R_min;

  output.Va_d = waypoints_[idx_a_].Va_d;
  output.r[0] = w_im1(0);
  output.r[1] = w_im1(1);
  output.r[2] = w_im1(2);
  Eigen::Vector3f q_im1 = (w_i - w_im1).normalized();
  Eigen::Vector3f q_i = (w_ip1 - w_i).normalized();
  float beta = acosf(-q_im1.dot(q_i));

  Eigen::Vector3f z;
  switch (fil_state_)
  {
  case fillet_state::STRAIGHT:
    output.flag = true;
    output.q[0] = q_im1(0);
    output.q[1] = q_im1(1);
    output.q[2] = q_im1(2);
    output.c[0] = 1;
    output.c[1] = 1;
    output.c[2] = 1;
    output.rho = 1;
    output.lambda = 1;
    z = w_i - q_im1*(R_min/tanf(beta/2.0));
    if ((p - z).dot(q_im1) > 0)
      fil_state_ = fillet_state::ORBIT;
    break;
  case fillet_state::ORBIT:
    output.flag = false;
    output.q[0] = q_i(0);
    output.q[1] = q_i(1);
    output.q[2] = q_i(2);
    Eigen::Vector3f c = w_i - (q_im1 - q_i).normalized()*(R_min/sinf(beta/2.0));
    output.c[0] = c(0);
    output.c[1] = c(1);
    output.c[2] = c(2);
    output.rho = R_min;
    output.lambda = ((q_im1(0)*q_i(1) - q_im1(1)*q_i(0)) > 0 ? 1 : -1);
    z = w_i + q_i*(R_min/tanf(beta/2.0));
    if ((p - z).dot(q_i) > 0)
    {
      if (idx_a_ == num_waypoints_ - 1)
        idx_a_ = 0;
      else
        idx_a_++;
      fil_state_ = fillet_state::STRAIGHT;
    }
    break;
  }
}

bool path_manager_example::isInHalfPlane(Eigen::Vector3f p,Eigen::Vector3f z,Eigen::Vector3f q)
{
  bool isIn = ((p - z).dot(q) > 0);
  if(isIn){
    ROS_WARN("is in half plane ");
  }
  return ((p - z).dot(q) > 0);
}


void path_manager_example::manage_dubins(const params_s &params, const input_s &input, output_s &output)
{
  Eigen::Vector3f p;
  p << input.pn, input.pe, -input.h;
  output.Va_d = waypoints_[idx_a_].Va_d;
  waypoint_s way0 = waypoints_[idx_a_];
  waypoint_s way1 = waypoints_[idx_a_+1];
  output.rho = params.R_min;
  // Determine the Dubins path parameters

  dubinsParameters(way0, way1, output.rho);
  // implement algorithm 8 in chapter 11

  switch (dub_state_)
  {
  case dubin_state::FIRST:
    idx_a_ = 0;
    output.flag = false;
    output.Va_d = 12;
    output.r[0] = 0;
    output.r[1] = 0;
    output.r[2] = 0;
    output.q[0] = 0;
    output.q[1] = 0;
    output.q[2] = 0;
    output.c[0] = 0.0f;
    output.c[1] = 0.0f;
    output.c[2] = -50.0f;
    output.rho = params.R_min;
    output.lambda = 1;
    ROS_WARN("Changing state to BEFORE_H1_WRONG_SIDE");     
    dub_state_ = dubin_state::BEFORE_H1_WRONG_SIDE;
    break;

  case dubin_state::BEFORE_H1_WRONG_SIDE:
    output.flag = false;
    output.r[0] = 0;
    output.r[1] = 0;
    output.r[2] = 0;
    output.q[0] = 0;
    output.q[1] = 0;
    output.q[2] = 0;
    output.c[0] = dubinspath_.cs[0];
    output.c[1] = dubinspath_.cs[1];
    output.c[2] = dubinspath_.cs[2];
    output.lambda = dubinspath_.lams;
    if (isInHalfPlane(p,dubinspath_.w1,-dubinspath_.q1)) {
      ROS_WARN("Changing state to BEFORE_H1");
      dub_state_ = dubin_state::BEFORE_H1;
    }
    break;

  case dubin_state::BEFORE_H1:
    output.flag = false;
    output.r[0] = 0;
    output.r[1] = 0;
    output.r[2] = 0;
    output.q[0] = 0;
    output.q[1] = 0;
    output.q[2] = 0;
    output.c[0] = dubinspath_.cs[0];
    output.c[1] = dubinspath_.cs[1];
    output.c[2] = dubinspath_.cs[2];
    output.lambda = dubinspath_.lams;
    if (isInHalfPlane(p,dubinspath_.w1,dubinspath_.q1)) {
      ROS_WARN("Changing state to STRAIGHT");
      dub_state_ = dubin_state::STRAIGHT;
    }
    break;

  case dubin_state::STRAIGHT:
    output.flag = true;
    output.r[0] = dubinspath_.w1[0];
    output.r[1] = dubinspath_.w1[1];
    output.r[2] = dubinspath_.w1[2];
    output.q[0] = dubinspath_.q1[0];
    output.q[1] = dubinspath_.q1[1];
    output.q[2] = dubinspath_.q1[2];
    output.c[0] = 0;
    output.c[1] = 0;
    output.c[2] = 0;
    output.lambda = dubinspath_.lame;
    if (isInHalfPlane(p,dubinspath_.w2,dubinspath_.q1)){
      ROS_WARN("Changing state to BEFORE_H3_WRONG_SIDE");
      dub_state_ = dubin_state::BEFORE_H3_WRONG_SIDE;
    }
    break;

  case dubin_state::BEFORE_H3_WRONG_SIDE:
    output.flag = false;
    output.r[0] = 0;
    output.r[1] = 0;
    output.r[2] = 0;
    output.q[0] = 0;
    output.q[1] = 0;
    output.q[2] = 0;
    output.c[0] = dubinspath_.ce[0];
    output.c[1] = dubinspath_.ce[1];
    output.c[2] = dubinspath_.ce[2];
    output.lambda = 0;
    if (isInHalfPlane(p,dubinspath_.w3,-dubinspath_.q3)){
      ROS_WARN("Changing state to BEFORE_H3");     
      dub_state_ = dubin_state::BEFORE_H3;
    }
    break;

  case dubin_state::BEFORE_H3:
    output.flag = false;
    output.r[0] = 0;
    output.r[1] = 0;
    output.r[2] = 0;
    output.q[0] = 0;
    output.q[1] = 0;
    output.q[2] = 0;
    output.c[0] = dubinspath_.ce[0];
    output.c[1] = dubinspath_.ce[1];
    output.c[2] = dubinspath_.ce[2];
    output.lambda = dubinspath_.lame;
    if (isInHalfPlane(p,dubinspath_.w3,dubinspath_.q3))
    {
      if (idx_a_ < num_waypoints_-1)
      {
        idx_a_++;
        ROS_WARN("Changing state to BEFORE_H1_WRONG_SIDE");     
        dub_state_ = dubin_state::BEFORE_H1_WRONG_SIDE;
      }
      else
      {
        ROS_WARN("Changing state to FIRST");     
        dub_state_ = dubin_state::FIRST;
      }
    }
    break;

  }
}

Eigen::Matrix3f path_manager_example::rotz(float theta)
{
  Eigen::Matrix3f R;
  R << cosf(theta), -sinf(theta), 0,
  sinf(theta),  cosf(theta), 0,
  0,            0, 1;

  return R;
}


float path_manager_example::angle(Eigen::Vector3f v)
{
  return atan2(v[1],v[0]);
}

float path_manager_example::mo(float in)
{
  float val;
  if (in > 0)
    val = fmod(in, 2.0*M_PI_F);
  else
  {
    float n = floorf(in/2.0/M_PI_F);
    val = in - n*2.0*M_PI_F;
  }
  return val;
}

void path_manager_example::dubinsParameters(const waypoint_s start_node, const waypoint_s end_node, float R)
{
  Eigen::Vector3f p_s(start_node.w);
  Eigen::Vector3f p_e(end_node.w);

  Eigen::Vector3f e_1;
  e_1 << 1.0, 0.0, 0.0;

  float chi_s = start_node.chi_d;
  float chi_e = end_node.chi_d;

  float ccs = cosf(chi_s);
  float scs = sinf(chi_s);
  float cce = cosf(chi_e);
  float sce = sinf(chi_e);

  Eigen::Vector3f qcs(ccs,scs,0);
  Eigen::Vector3f qce(cce,sce,0);

// Compute circle centers
  Eigen::Vector3f c_rs = p_s + R*rotz(M_PI_2_F)*qcs;
  Eigen::Vector3f c_ls = p_s + R*rotz(-M_PI_2_F)*qcs;
  Eigen::Vector3f c_re = p_e + R*rotz(M_PI_2_F)*qce;
  Eigen::Vector3f c_le = p_e + R*rotz(-M_PI_2_F)*qce;
// Compute path lengths
// Case 1: R-S-R
  float th = angle(c_re - c_rs);
  float L1 = (c_rs - c_re).norm() +
             R*mo(M_2_PI_F + mo(th - M_PI_2_F) - mo(chi_s - M_PI_2_F)) +
             R*mo(M_2_PI_F - mo(th - M_PI_2_F) + mo(chi_e - M_PI_2_F));
// Case 2: R-S-L
  th = angle(c_le - c_rs);
  float ell = (c_le - c_rs).norm();
  float th2 = th - M_PI_2_F + asinf(2*R/ell);

  float L2 = sqrt(ell*ell - 4*R*R) +
             R*mo(M_2_PI_F + mo(th2) - mo(chi_s - M_PI_2_F)) +
             R*mo(M_2_PI_F + mo(th2 + M_PI_F) - mo(chi_e + M_PI_2_F));

// Case 3: L-S-R
  th = angle(c_re - c_ls);
  ell = (c_re - c_ls).norm();
  th2 = acosf(2*R/ell);

  float L3 = sqrt(ell*ell - 4*R*R) +
             R*mo(M_2_PI_F - mo(th + th2) + mo(chi_s + M_PI_2_F)) +
             R*mo(M_2_PI_F - mo(th + th2 - M_PI_F) + mo(chi_e - M_PI_2_F));
// Case 4: L-S-L
  th = angle(c_le - c_ls);
  float L4 = (c_ls - c_le).norm() +
             R*mo(M_2_PI_F - mo(th + M_PI_2_F) + mo(chi_s + M_PI_2_F)) +
             R*mo(M_2_PI_F + mo(th + M_PI_2_F) - mo(chi_e + M_PI_2_F));
// Define the parameters for the minimum length path (i.e. Dubins path)
  float L = L1;
  int i_min = 1;
  if (L2 < L)
  {
    L = L2;
    i_min = 2;
  }
  if (L3 < L)
  {
    L = L3;
    i_min = 3;
  }
  if (L4 < L)
  {
    L = L4;
    i_min = 4;
  }

  Eigen::Vector3f c_s;
  Eigen::Vector3f c_e;
  int lambda_s;
  int lambda_e;
  Eigen::Vector3f z_1;
  Eigen::Vector3f q_1;
  Eigen::Vector3f z_2;
  Eigen::Vector3f z_3;
  Eigen::Vector3f q_3;
// Implement algorithm 7
  if (i_min == 1)
  {
    c_s = c_rs;
    lambda_s = 1;
    c_e = c_re;
    lambda_e = 1;
    q_1 = (c_e - c_s).normalized();
    z_1 = c_s + R*rotz(-M_PI_2_F) * q_1;
    z_2 = c_e + R*rotz(-M_PI_2_F) * q_1;
  }
  else if (i_min == 2)
  {
    c_s = c_rs;
    lambda_s = 1;
    c_e = c_le;
    lambda_e = -1;
    ell = (c_e - c_s).norm();
    th = angle(c_e - c_s);
    th2 = mo(th - M_PI_2_F + asinf(2*R/ell));
    q_1 = rotz(th2 + M_PI_2_F)*e_1;
    z_1 = c_s + R*rotz(th2) * e_1;
    z_2 = c_e + R*rotz(th2 + M_PI_F) * e_1;
  }
  else if (i_min == 3)
  {
    c_s = c_ls;
    lambda_s = -1;
    c_e = c_re;
    lambda_e = 1;
    ell = (c_e - c_s).norm();
    th = angle(c_e - c_s);
    th2 = acosf(2*R/ell);
    q_1 = rotz(th + th2 - M_PI_2_F)*e_1;
    z_1 = c_s + R*rotz(th + th2) * e_1;
    z_2 = c_e + R*rotz(th + th2 - M_PI_F) * e_1;
  }
  else     // i_min == 4
  {
    c_s = c_ls;
    lambda_s = -1;
    c_e = c_le;
    lambda_e = -1;
    q_1 = (c_e - c_s).normalized();
    z_1 = c_s + R*rotz(M_PI_2_F) * q_1;
    z_2 = c_e + R*rotz(M_PI_2_F) * q_1;
  }
  z_3 = p_e;
  q_3 = rotz(chi_e)*e_1;

// Package outputs into struct
  dubinspath_.ps = p_s;
  dubinspath_.chis = chi_s;
  dubinspath_.ps = p_e;
  dubinspath_.chie = chi_e;
  dubinspath_.R = R;
  dubinspath_.L = L;

  dubinspath_.cs = c_s;
  dubinspath_.lams = lambda_s;
  dubinspath_.ce = c_e;
  dubinspath_.lame = lambda_e;
  dubinspath_.w1 = z_1;
  dubinspath_.q1 = q_1;
  dubinspath_.w2 = z_2;
  dubinspath_.w3 = z_3;
  dubinspath_.q3 = q_3;
}

}//end namespace
