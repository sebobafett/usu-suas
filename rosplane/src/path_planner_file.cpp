#include <ros/ros.h>
#include <rosplane_msgs/Waypoint.h>
#include <ros/package.h>
#include <fstream>


int main(int argc, char **argv)
{
  ros::init(argc, argv, "rosplane_simple_path_planner");
  ros::NodeHandle nh_;
  ros::Publisher waypointPublisher = nh_.advertise<rosplane_msgs::Waypoint>("waypoint_path", 10);

  std::string fname, commentline;
  if (!nh_.hasParam("waypoint_file"))
  {
    nh_.setParam("waypoint_file", ros::package::getPath("rosplane") + "/waypoints.txt");
  }
  nh_.getParam("waypoint_file", fname);

  std::ifstream fin(fname);
  if (!fin.is_open())
  {
    ROS_FATAL("Unable to open file: '%s'", fname.c_str());
    return 1;
  }

  std::string w[3], chi_d, Va_d;
  bool first = true;

  while (!fin.eof())
  {
    fin >> w[0];
    if (w[0][0] == '/' && w[0][1] == '/')
    {
      // throw away the commments
      getline(fin, commentline);
      continue;
    }

    fin >> w[1] >> w[2] >> chi_d >> Va_d;
    ros::Duration(0.5).sleep();

    rosplane_msgs::Waypoint new_waypoint;

    new_waypoint.w[0] = std::stof(w[0]);
    new_waypoint.w[1] = std::stof(w[1]);
    new_waypoint.w[2] = std::stof(w[2]);
    new_waypoint.chi_d = std::stof(chi_d) * M_PI / 180; // to radians

    new_waypoint.chi_valid = true;
    new_waypoint.Va_d = std::stof(Va_d);
    if (first)
    {
      new_waypoint.set_current = true;
      first = false;
    }
    else
    {
      new_waypoint.set_current = false;
    }
    new_waypoint.clear_wp_list = false;

    waypointPublisher.publish(new_waypoint);
  }
  ros::Duration(1.5).sleep();

  return 0;
}
